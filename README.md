# Angular CDP Demo

This application is a small demo how to use Angular CDP components. It contains all needed dependencies to get you
started.

## Installation

Clone the repository recursively (`git clone --recursive ...`) and run

    npm install

This will download all dependencies.

## Usage

Open `app/index.html` and see the code inside `<body>` element. Modify this to suit the needs of your own application.

## Integrating with CDP

Consult the manual of [CDP Studio](http://cdpstudio.com/) to create a CDP Application with a web server.

Copy the content of `app` directory of this project under `www` directory of your CDP Application 
and run the Application.

Navigate with your browser to the address printed by CDP Application.
Usually it is `127.0.0.1:7689/www/index.html`.
When connecting to a remote controller use a different IP address.

Now you should see the web UI of your control system.

## Documentation

All Angular CDP components are documented [here](https://gitlab.com/karmo/angular-cdp/).
